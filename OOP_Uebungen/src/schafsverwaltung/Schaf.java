package schafsverwaltung;

public class Schaf {

	private String name;
	private byte alter;
	private float wollMenge;

	public Schaf() {
		System.out.println("Im parameterlosen Konstruktor 1.");
	}

	public Schaf(String name) {
		this.name = name;
		System.out.println("Im Konstruktor 2.");
	}

	public Schaf(String name, byte alter, float wolle) {
		this(name);
		this.alter = alter;
		wollMenge = wolle;
		System.out.println("Im Konstruktor 3.");
	}

	public static void main(String[] args) {
		System.out.println("\n--- Instanziere objekt1 ---");
		Schaf objekt1 = new Schaf();
		System.out.println("\n--- Instanziere objekt2 ---");
		Schaf objekt2 = new Schaf("Othello");
		System.out.println("\n--- Instanziere objekt3 ---");
		Schaf objekt3 = new Schaf("Cloud", (byte) 4, 2.15F);
		System.out.println();
		objekt1.merkmaleAusgeben("objekt1");
		objekt2.merkmaleAusgeben("objekt2");
		objekt3.merkmaleAusgeben("objekt3");
	}

	public void merkmaleAusgeben(String objektName) {
		System.out.println("\n- Ausgabe der Merkmale von " + objektName + " -");
		System.out.println("Name: " + name);
		System.out.println("Alter: " + alter + " Jahre");
		System.out.println("Wollmenge: " + wollMenge + " m^2");
	}
}
