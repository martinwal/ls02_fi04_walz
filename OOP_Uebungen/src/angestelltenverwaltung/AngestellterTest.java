package angestelltenverwaltung;

public class AngestellterTest {
	public static void main(String[] args) {
		Angestellter ang1 = new Angestellter();
		Angestellter ang2 = new Angestellter("Meier");
		Angestellter ang3 = new Angestellter("Schmid", 2000);
		
		System.out.printf("Name: %1s / Gehalt: %2.2f\n", ang1.getName(), ang1.getGehalt());
		System.out.printf("Name: %1s / Gehalt: %2.2f\n", ang2.getName(), ang2.getGehalt());
		System.out.printf("Name: %1s / Gehalt: %2.2f\n", ang3.getName(), ang3.getGehalt());
	}
}
