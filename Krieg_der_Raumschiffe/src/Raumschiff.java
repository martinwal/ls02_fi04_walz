import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {

	private int photonentorpedoAnzahl, androidenAnzahl, schildeInProzent, huelleInProzent,
			lebenserhaltungssystemeInProzent, energieversorgungInProzent;
	private String schiffsName;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsVerzeichnis;

	public Raumschiff() {
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungsVerzeichnis = new ArrayList<Ladung>();
	}

	public Raumschiff(String name, int photonentorpedoAnzahl, int androidenAnzahl, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int energieversorgungInProzent) {
		this();
		this.schiffsName = name;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.androidenAnzahl = androidenAnzahl;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public String getSchiffsname() {
		return this.schiffsName;
	}

	public void setSchiffsname(String name) {
		if (name != "")
			this.schiffsName = name;
		else
			System.out.println("Der Schiffsname darf nicht leer sein! Bite Eingabe wiederholen");
	}

	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}

	public void setAndroidenAnzahl(int anzahl) {
		this.androidenAnzahl = Math.max(0, anzahl);
	}

	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int anzahl) {
		this.photonentorpedoAnzahl = Math.max(0, anzahl);
	}

	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}

	public void setSchildeInProzent(int neuerWert) {
		if (neuerWert < 1) {
			this.schildeInProzent = 0;
		} else {
			this.schildeInProzent = Math.min(neuerWert, 100);
		}
	}

	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}

	public void setHuelleInProzent(int neuerWert) {
		if (neuerWert < 1) {
			this.huelleInProzent = 0;
		} else {
			this.huelleInProzent = Math.min(neuerWert, 100);
		}
	}

	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int neuerWert) {
		if (neuerWert < 1) {
			this.lebenserhaltungssystemeInProzent = 0;
		} else {
			this.lebenserhaltungssystemeInProzent = Math.min(neuerWert, 100);
		}
	}

	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int neuerWert) {
		if (neuerWert < 1) {
			this.energieversorgungInProzent = 0;
		} else {
			this.energieversorgungInProzent = Math.min(neuerWert, 100);
		}
	}

	// AUFGABEN
	public void addLadung(Ladung neueLadung) {
		this.ladungsVerzeichnis.add(neueLadung);
	}

	public void zeigeLadungverzeichnis() {
		System.out.println("--------------");
		System.out.println("Ladungsverzeichnis von " + this.schiffsName);
		System.out.println("Anzahl der Ladungen: " + this.ladungsVerzeichnis.size());
		for (Ladung x : this.ladungsVerzeichnis) {
			System.out.println("- " + x.getBezeichnung() + " - Menge: " + x.getMenge());
		}
		System.out.println("--------------");
	}

	public void zeigeSchiffszustand() {
		System.out.println("--------------");
		System.out.println("Schiffszustand von " + this.schiffsName);
		System.out.println("Schilde: " + this.schildeInProzent + "%");
		System.out.println("H�lle: " + this.huelleInProzent + "%");
		System.out.println("Energie: " + this.energieversorgungInProzent + "%");
		System.out.println("Lebenserhaltung: " + this.lebenserhaltungssystemeInProzent + "%");
		System.out.println("Photonentorpedos: " + this.photonentorpedoAnzahl);
		System.out.println("Androiden: " + this.androidenAnzahl);
		System.out.println("--------------");
	}

	public void schiessePhotonenTorpedo(Raumschiff schiff) {
		if (this.photonentorpedoAnzahl > 0) {
			this.photonentorpedoAnzahl--;
			sendeNachrichtanAlle("Photonentorpedo abgeschossen");
			Treffer(schiff);
		} else
			sendeNachrichtanAlle("-=*Click*=-");
	}

	public void schiessePhaserkaonen(Raumschiff schiff) {
		if (this.energieversorgungInProzent >= 50) {
			sendeNachrichtanAlle("Phaserkanone abgeschossen");
			this.energieversorgungInProzent -= 50;
			Treffer(schiff);
		} else
			sendeNachrichtanAlle("-=*Click*=-");
	}

	public void Treffer(Raumschiff schiff) {
		System.out.println(schiff.schiffsName + " wurde getroffen!");
		if (schiff.getSchildeInProzent() > 0) {
			schiff.setSchildeInProzent(schiff.getSchildeInProzent() - 50);
		} else {
			if (schiff.getHuelleInProzent() > 0) {
				schiff.setHuelleInProzent(schiff.getHuelleInProzent() - 50);
				schiff.setEnergieversorgungInProzent(schiff.getEnergieversorgungInProzent() - 50);
			} else {
				schiff.setLebenserhaltungssystemeInProzent(0);
				sendeNachrichtanAlle("Lebenserhaltungssysteme wurden vernichtet!");
			}
		}
	}

	public void photonentorpedoEinsetzen(int einzusetzendeTorpedos) {
		boolean torpedosVorhanden = false;
		for (Ladung x : this.ladungsVerzeichnis) {
			if (x.getBezeichnung() == "Photonentorpedos" && x.getMenge() > 0) {
				torpedosVorhanden = true;
				int i = Math.min(x.getMenge(), einzusetzendeTorpedos);
				this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + i);
				x.setMenge(x.getMenge() - i);
				sendeNachrichtanAlle(i + " Photonentorpedo(s) eingesetzt");
			}
		}
		if (!torpedosVorhanden) {
			System.out.println("Keine Photonentorpedos gefunden!");
			sendeNachrichtanAlle("-=*Click*=-");
		}
	}

	public void ladungAufraeumen() {
		for (int i = this.ladungsVerzeichnis.size() - 1; i >= 0; i--) {
			if (this.ladungsVerzeichnis.get(i).getMenge() == 0)
				this.ladungsVerzeichnis.remove(i);
		}
	}

	public void zeigeLog() {
		System.out.println("Log des Boradcastkommunikators:");
		for (String s : this.broadcastKommunikator)
			System.out.println(s);
	}

	public void sendeNachrichtanAlle(String text) {
		this.broadcastKommunikator.add(text);
		System.out.println(text);
	}

	public void androidenEinsetzen(int anzahlDroiden, boolean huelle, boolean schild, boolean energie) {
		if (anzahlDroiden > this.androidenAnzahl)
			anzahlDroiden = this.androidenAnzahl;
		Random rnd = new Random();

		int x = (rnd.nextInt(100) * anzahlDroiden) / (((huelle) ? 1 : 0) + ((schild) ? 1 : 0) + ((energie) ? 1 : 0));
		if (huelle)
			this.setHuelleInProzent(this.getHuelleInProzent() + x);
		if (schild)
			this.setSchildeInProzent(this.getSchildeInProzent() + x);
		if (energie)
			this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent() + x);
	}
}
