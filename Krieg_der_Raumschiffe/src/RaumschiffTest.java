
public class RaumschiffTest {
	public static void main(String[] args) {

		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 1, 2, 100, 100, 100, 100);
		klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));

		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 2, 100, 100, 100, 100);
		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung("Rote Materie", 2));
		romulaner.addLadung(new Ladung("Plasma-Waffe", 50));

		Raumschiff vulkanier = new Raumschiff("Ni'Var", 0, 5, 80, 50, 100, 80);
		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		vulkanier.addLadung(new Ladung("Photonentorpedos", 3));

		// Die Klingonen schie�en mit dem Photonentorpedo einmal auf die Romulaner.
		klingonen.schiessePhotonenTorpedo(romulaner);
		// Die Romulaner schie�en mit der Phaserkanone zur�ck.
		romulaner.schiessePhaserkaonen(klingonen);
		// Die Vulkanier senden eine Nachricht an Alle �Gewalt ist nicht logisch�.
		vulkanier.sendeNachrichtanAlle("Gewalt ist nicht logisch");
		// Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr
		// Ladungsverzeichnis aus
		klingonen.zeigeSchiffszustand();
		klingonen.zeigeLadungverzeichnis();

		// Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur
		// Aufwertung ihres Schiffes ein (f�r Experten).
		vulkanier.androidenEinsetzen(vulkanier.getAndroidenAnzahl(), true, true, true);

		// Die Vulkanier verladen Ihre Ladung �Photonentorpedos� in die Torpedor�hren
		// Ihres Raumschiffes und r�umen das Ladungsverzeichnis auf (f�r Experten).
		vulkanier.photonentorpedoEinsetzen(3);
		vulkanier.ladungAufraeumen();

		// Die Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner.
		klingonen.schiessePhotonenTorpedo(romulaner);
		klingonen.schiessePhotonenTorpedo(romulaner);

		// Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand
		// Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
		klingonen.zeigeSchiffszustand();
		klingonen.zeigeLadungverzeichnis();

		romulaner.zeigeSchiffszustand();
		romulaner.zeigeLadungverzeichnis();

		vulkanier.zeigeSchiffszustand();
		vulkanier.zeigeLadungverzeichnis();
	}
}
